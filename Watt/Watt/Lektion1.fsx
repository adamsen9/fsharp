﻿// LEKTION 1
// Opgave 1
let rec f n =
    match n with
    | 0 -> 0
    | n -> n + f(n - 1)

f 4
//Opgave 2
let rec sum = function
    | (m,0) -> m
    | (m,n) -> m + n + sum(m, n-1)

sum (2,2)
//Opgave 3

//Opgave 4
type testtype = (int * int list)

let rec multiplicity (input:testtype) =
    let (x , ys) = input //Eksempel på udtræk af data fra par
    match ys with
    | [] -> 0
    | _ when ys.Head = x -> 1 + multiplicity (x, ys.Tail)
    | _ -> 0 + multiplicity (x, ys.Tail)

multiplicity (2, [2; 4; 2; 10; 1; 2])

//Opgave 5
let rec mulC (x, y) =
    match y with
    | [] -> []
    | _ -> y.Head * x :: mulC (x, y.Tail)

mulC (3, [2; 4; 2; 10; 1; 2])

//Opgave 6
type addEtype = int list * int list
let rec addE (input:addEtype) =
    let x,y = input
    match x,y with
    | ([],[]) -> []
    | (_,[]) -> x
    | ([],_) -> y
    | (_,_) -> x.Head + y.Head :: addE (x.Tail,y.Tail)

let res1 = addE([1; 2; 3], [4; 5; 6]) = [5; 7; 9]
let res2 = addE([1; 2], [3; 4; 5; 6]) = [4; 6; 5; 6]
let res3 = addE([10; 2; 3; 4], [5; 25])

//Opgave 7
let mulX input =
    0 :: input

let res4 = mulX (mulX [1; 2; 3])

//Lektion 2
// 2.1
let f1 x =
    match x with
    | _ when x % 5 = 0 -> false
    | _ when x % 2 = 0 -> true
    | _ when x % 3 = 0 -> true
    | _ -> false

let res5 = f1 2
let res6 = f1 4
let res7 = f1 10
let res8 = f1 14
let res9 = f1 18
let res10 = f1 20

// 2.2
let rec pow ((s:string), (n:int)) =
    match n with
    | 0 -> []
    | n when n > 0 -> s :: pow (s, (n - 1))
    | _ -> s :: []

let res11 = pow ("hej", 5)
let res12 = pow ("lol", 20)

//2.3
let isIthChar (s:string, n:int, c:char) =
    [for c in s -> c] |> List.item n = c // konverterer string til liste af chars

let res13 = isIthChar ("hullabulla", 3, 'l')
let res14 = isIthChar ("hullabulla", 5, 'l')

//4.1
let upto n =
    [1 .. n ]

let res01 = upto 5
let res02 = upto 52

//4.2
let downto1 n =
    [n.. -1 .. 1]

let res03 = downto1 10
let res04 = downto1 5

//4.3
let evenN n =
    [2.. 2 .. n*2]


let res15 = evenN 5 |> List.length = 5
let res16 = evenN 33 |> List.length = 33

//4.5
let rmodd n =
    let mutable x = 0
    let mutable y = []
    for i in n do
        if x % 2 = 0 then y <- i :: y
        x <- x + 1
    List.rev y

let res17 = rmodd [5; 7; 9; 10; 20; 2; 2; 2; 2; 2; 4]


//4.6
let rec removeEven n =
    match n with
    | [] -> []
    | n when n.Head % 2 <> 0 ->  n.Head :: removeEven n.Tail
    | _ -> removeEven n.Tail

let res18 = removeEven [4; 6; 5; 6; 7; 0;6;8;10;12;11;13;15]

//4.7
let multiplicity2 x xs =
    let mutable count = 0
    for i in xs do
        if i = x then count <- count + 1
    count

let res19 = multiplicity2 6 [4; 6; 5; 6; 7; 0;6;8;10;12;11;13;15]

//4.8
let split n =
    let mutable x = 0
    let mutable y = []
    let mutable z = []
    for i in n do
        if x % 2 = 0 then y <- i :: y else z <- i :: z
        x <- x + 1
    y <- List.rev y
    z <- List.rev z
    (y, z)

let res20 = split [1; 2;56;123;454;123;689;1234465;1345435;123;89;134]

//4.9
let zip (x, y) =
    if List.length x <> List.length y then failwith "Different derps"
    let mutable z = []
    let mutable count = 0
    for i in x do
        z <- [(List.item count x, List.item count y)] :: z
        count <- count + 1
    List.rev z

let res21 = zip ([1;2;3],[4;5;6])

//4.12
let rec p_sum ((p:int -> bool),(xs:int list)) =
    match xs with
    | [] -> []
    | _ when p xs.Head -> xs.Head :: p_sum (p,xs.Tail)
    | _ -> p_sum (p,xs.Tail)

let predicate1 x = x > 10
let predicate2 x = x % 2 = 0
let predicate3 x = x % 2 <> 0
let res22 = p_sum (predicate1,[1;2;3;11;12;10;20])
let res23 = p_sum (predicate2,[1;2;3;11;12;10;20])
let res24 = p_sum (predicate3,[1;2;3;11;12;10;20])