﻿//Exam, may 29th 2015
//Task 1
//Problem 1
let rec repeat s x =
    match x with
    | 0 -> ""
    | _ -> s + repeat s (x-1)

//Problem 2
let f s1 s2 n =
    let mutable s = ""
    for i in [0..n] do
        if i % 2 = 0 then s <- s + s1 + "\n" else s <- s + s2 + "\n"
    s

let res = f "yo" "lo" 10

//Problem 3
let viz' s n =
    let mutable s' = ""
    for i in [0..n] do s' <- s' + s
    s'

let viz m n =
    let mutable s = ""
    for i in [0..m] do
        if i % 2 = 0 then s <- s + (viz' "XO" n) + "\n" else s <- s + (viz' "OX" n) + "\n"
    s

printfn "%s" (viz 4 5)

//Task 2
//Problem 1
let rec f' i = function
    | [] -> []
    | x::xs -> (x+i)::f' (i*i) xs

let res2 = f' 2 [1;2;3;4]

//Task 3

//Task 4
//Problem 1
let rec repeatList xs n =
    match n with
    | 0 -> []
    | _ -> List.append xs (repeatList xs (n-1))

let res41 = repeatList [1;2] 3
let res42 = repeatList [1;2] 0

//Problem 2
let rec merge x y =
    match x,y with
    | ([],[]) -> []
    | (_,[]) -> x
    | ([],_) -> y
    | (_,_) -> x.Head :: y.Head :: merge x.Tail y.Tail

let res43 = merge [1; 2; 3] [4; 5; 6]
let res44 = merge [1; 2] [3; 4; 5; 6]
let res45 = merge [10; 2; 3; 4]  [5; 25]

