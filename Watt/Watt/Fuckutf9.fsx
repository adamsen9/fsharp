﻿//Inv
let inv (x:('a*'a*int) list) =

    let rec invAssist sbs y =
        match sbs with
        | [] -> true
        | _ ->
            let (_,_,z) = sbs.Head
            if z <= y then invAssist sbs.Tail z else false
    invAssist x 2147483647

let sb = [("Joe", "June Fishing", 35); ("Peter", "May Fishing", 30); ("Joe", "May Fishing", 28); ("Paul", "June Fishing", 28)]

inv sb

//HNNNNNG

//Records
type Person = {age: int; birthday: int * int; name : string; sex: string}
let john = {name = "John"; age = 29; sex = "M"; birthday = (2,11)}

//PUNKTUM-NOTATION
john.birthday 
john.sex

//VBLARGH
Set.foldBack (-) (set[1;2;3]) 0

//TEST
type Name = string
type Flow = int
type River = R of Name * Flow * Tributaries
and Tributaries = River list

let riv1 = R ("R1", 5, [])
let riv4 = R ("R4", 2, [])
let riv3 = R ("R3", 8, [])
let riv2 = R ("R2", 15, [riv4])
let riv = R ("R",10, [riv1; riv2; riv3])

let contains (name:Name) (river:River) =

    let rec helper (x:Tributaries) = 
        match x with
        | [] -> false
        | _ -> match x.Head with
                | R(u,_,z) -> u = name || helper z || helper x.Tail

    match river with
    | R(a,_,c) -> 
        if a = name then true else helper c
            
let res = contains "R" riv

//All names
let allNames (river:River) =

    let rec helper (x:Tributaries) = 
        match x with
        | [] -> []
        | _ -> match x.Head with
                | R(u,_,z) -> u :: helper x.Tail @ helper z

    match river with
    | R(a,_,c) -> a :: helper c
            
let res1 = allNames riv



//Totalflow
let totalFlow (river:River) =

    let rec helper (x:Tributaries) = 
        match x with
        | [] -> 0
        | _ -> match x.Head with
                | R(_,u,z) -> u + helper z + helper x.Tail

    match river with
    | R(_,a,c) -> 
        a + helper c
            
let res3 = totalFlow riv

//mainSource
let mainSource (river:River) =
    
    let rec helper (x:Tributaries) = 
        match x with
        | [] -> []
        | _ -> match x.Head with
                | R(u,p,z) -> (u, p) :: helper x.Tail @ helper z

    match river with
    | R(a,b,c) -> (a, b) :: helper c |> List.maxBy snd
                    
let res4 = mainSource riv

//tryInsert
let tryInsert (name:Name) (river:River) (river':River) =
    match river' with
    | _ when contains name river' -> Some river'
    | _ -> None


let res5 = tryInsert