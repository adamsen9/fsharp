﻿//Lektion 4
let rec p q = function
    | [] -> []
    | x::xs -> 
        let ys = p q xs
        if q x then x::ys else ys@[x];

let predicate x = x < 5
let list = [1;2;3;4;5;6;7;8;9]
let list2 = [1.0;2.0;3.0;4.0;5.0;6.0;7.0;8.0;9.0]

let res1 = p predicate list

let rec f g = function
    | [] -> []
    | x::xs -> g x :: f (fun y -> g(g y)) xs

let functiong x = x*2.0

let res2 = f functiong list2
