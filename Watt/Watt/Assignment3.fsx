//Frederik Skovb�k Adamsen s123157
//Mathias Petersen s144874
//Nicolai Hansen s133974
//Every member contributed roughly equally

type Name = string
type Flow = int
type Tributaries = River list
and River = | Mouth of Name * Flow | Source of Name * Flow * Tributaries

//3.1
let riv: River = Source("R", 10, [Mouth("R1", 5); Source("R2", 15, [Mouth("R4", 2)]); Mouth("R3", 8)])
let riv3: River = Mouth("R3", 8)
let riv1: River = Mouth("R1", 5)
let riv2: River = Source("R2", 15, [Mouth("R4", 2)])
let riv4: River = Mouth("R4", 2)

//3.2 
let rec namesTributaries = function
    |[] -> []
    | e::es -> (allNames e) @ (namesTributaries es)
and allNames = function //<- 3.3
    | Mouth(n, f) -> [n]
    | Source(n, f, t) -> n :: (namesTributaries t)

let contains (name: Name) (river: River) =
    allNames river |> List.contains name

let test21 = contains "R3" riv3
let test22 = contains "R4" riv
let test23 = contains "R2" riv
let test24 = contains "R" riv
let test25 = contains "R4" riv2

//3.3 tests
let test31 = allNames riv = ["R"; "R1"; "R2"; "R4"; "R3"]
let test32 = allNames riv1 = ["R1"]
let test33 = allNames riv2 = ["R2"; "R4"]
let test34 = allNames riv3 = ["R3"]
let test35 = allNames riv4 = ["R4"]

//3.4
let rec flowTributaries = function
    |[] -> []
    | e::es -> (flowSource e) @ (flowTributaries es)
and flowSource = function
    | Mouth(n, f) -> [f]
    | Source(n, f, t) -> f :: (flowTributaries t)

let totalFlow (river: River) =
    flowSource river |> List.sum

let test41 = totalFlow riv = 40
let test42 = totalFlow riv1 = 5
let test43 = totalFlow riv2 = 17
let test44 = totalFlow riv3 = 8
let test45 = totalFlow riv4 = 2


//3.5
let rec flowMouth' = function
    |[] -> []
    | e::es -> (flowSource' e) @ (flowMouth' es)
and flowSource' = function
    | Mouth(n, f) -> [(n, f)]
    | Source(n, f, t) -> (n, f) :: (flowMouth' t)

let mainSource (river:River) =
   flowSource' river |> List.maxBy snd

let test51 = mainSource riv = ("R2", 15)
let test52 = mainSource riv3 = ("R3", 8)
let test53 = mainSource riv2 = ("R2", 15)

//3.6
let tryInsert (n:Name) (existingRiver:River) (newRiver:River) = 
    if contains n existingRiver then existingRiver
    else newRiver

let test61 = tryInsert "R1" riv (River.Mouth("R4", 2))

// 3.7
// Rivers cannot have more than one mouth in the above implementation.
// With the above implementation its rather tiresome adding more attributes to a river (like length, height-difference between source and mouth and so on).
// Declaration of the riversystem with this implementation has to be top down, s� if riv contains R3, one first has to declare R3
// The search-time in our riversystem is O(n), which is subpar for a tree-like structure