﻿//Dating site
type name = string
type tlfno = string
type sex = bool
type yob = int
type interest = string
type themes = interest list
type Entry = name * tlfno * sex * yob * themes
type request = sex * yob * themes
type database = Entry list

let commonElements a b =
    let rec commonHelper x y =
        match (x,y) with
        | ([],_) -> false
        | (_,[]) -> commonHelper x.Tail b
        | (_,_) -> if x.Head = y.Head then true else commonHelper x y.Tail
    commonHelper a b
let responseMachine (request:request)(db:database) =
    let (a,b,c) = request

    let predicateYob ((_,_,_,x,_):Entry) = (b - 10 < x) || (b + 10 > x)
    let predicateSex ((_,_,x,_,_):Entry) = a <> x
    let predicateInterest ((_,_,_,_,x):Entry) = commonElements c x

    List.filter predicateYob db |> List.filter predicateSex  |> List.filter predicateInterest


let db:database = [("Mr Swag","934756934",true, 1993, ["sports";"yolo"]); ("Ms Swag","934756934",false, 1994, ["sports"]);("Ms Yolo","934756934",false, 1992, ["esport"])]
let stud:request = (true, 1993, ["sports"])

let resx = responseMachine stud db
