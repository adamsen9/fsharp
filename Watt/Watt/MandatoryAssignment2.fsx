﻿//Frederik Skovbæk Adamsen s123157
//Mathias Petersen s144874
//Nicolai Hansen s133974
//This script was developed using triple programming (as opposed to pair programming), and as such every member roughly contributed equally

type CourseNo   = int
type Title      = string
type ECTS       = int
type CourseDesc = Title * ECTS 

type CourseBase = Map<CourseNo, CourseDesc>

type Mandatory   = Set<CourseNo>
type Optional    = Set<CourseNo> 
type CourseGroup = Mandatory * Optional

type BasicNaturalScience      = CourseGroup
type TechnologicalCore        = CourseGroup
type ProjectProfessionalSkill = CourseGroup
type Elective                 = CourseNo -> bool

type FlagModel  = BasicNaturalScience * TechnologicalCore 
                   * ProjectProfessionalSkill * Elective                 
type CoursePlan = Set<CourseNo>   
  
type CourseBaseAsList = (CourseNo * CourseDesc) list

 // 1. function isValidCourseDesc
 // tjek at ECTS % 5 == 0
let isValidCourseDesc (CourseDesc:CourseDesc) =
    snd CourseDesc % 5 = 0

 // 2. function isValidCourseBase
 // kalder rekursivt isValidCourseDesc for hver courseDesc i CourseBase, hvis ÉN er false, returner false
let rec validCourseBase (CourseBaseAsList:CourseBaseAsList) =
    match CourseBaseAsList with
    | [] -> true // basecase, returner true hvis liste
    | x::xs -> snd x |> isValidCourseDesc && validCourseBase xs

let isValidCourseBase (CourseBase:CourseBase) =
      Map.toList CourseBase |> validCourseBase

//3. function disjoint
// recursively, for each element in one list, check all elements in other list. If any have the same value, return false. Else, return true.
// use inbuilt intersect, if size of resulting set is 0, they are disjoint

let disjoint (SetX:Set<'a>) (SetY:Set<'a>) =
    Set.isEmpty (Set.intersect SetX SetY)

//4. function ects
// sum the ects values of the courses corresponding to the courseNo given
// foreach entry in values, find the ects-value in coursebase, add it to a sum, pass the rest of the list and the sum to itself recursively
let sumECTS (values: Set<CourseNo>) (courseBase: CourseBase) =
    let rec sum (values:list<CourseNo>) accum:int =
        match values with
        | [] -> accum
        | head :: tail -> sum tail (accum + (Map.find head courseBase |> snd ))
    sum (Set.toList values) 0

//5. function isValidCourseGroup
let isValidCourseGroup (group:CourseGroup) (courseBase:CourseBase) =
    let man = fst group
    let opt = snd group

    let manSum = sumECTS man courseBase
    let optSum = sumECTS opt courseBase

    disjoint man opt
    && manSum <= 45
    && ((optSum = 0 && manSum >= 45) || (manSum < 45))
    && manSum + optSum >= 45



// 6. isValid
let isValid (flagModel:FlagModel) (courseBase:CourseBase) = 
    let bns, tc, pps, ep = flagModel

    let megaIntersect = Set.intersect (fst bns) <| (Set.intersect (snd bns) <| (Set.intersect (fst tc) <| (Set.intersect (snd tc) <| (Set.intersect (fst pps)(snd pps) ))))
    let unionOfAllCoursegroups = Set.union (fst bns) <| (Set.union (snd bns) <| (Set.union (fst tc) <| (Set.union (snd tc) <| (Set.union (fst pps)(snd pps) ))))
    
    isValidCourseGroup bns courseBase
    && isValidCourseGroup tc courseBase
    && isValidCourseGroup pps courseBase
    && megaIntersect.Count = 0
    && Set.forall ep unionOfAllCoursegroups
    


// 7.


let checkPlan (plan:CoursePlan) (flagModel:FlagModel) (courseBase:CourseBase) =
    let bns, tc, pps, ep = flagModel
    let bnsMan, bnsOp = bns
    let tcMan, tcOp = tc
    let ppsMan, ppsOp = pps

    //Devide plan in to groups
    let bnsGroup: CourseGroup = (Set.intersect plan bnsMan, Set.intersect plan bnsOp)
    let tcGroup: CourseGroup = (Set.intersect plan tcMan, Set.intersect plan tcOp)
    let ppsGroup: CourseGroup = (Set.intersect plan ppsMan, Set.intersect plan ppsOp)

    //Rest of the course that isn't in a group
    let rest = Set.difference (fst bns) <| (Set.difference (snd bns) <| (Set.difference (fst tc) <| (Set.difference (snd tc) <| (Set.difference (snd pps) <| Set.difference (fst pps) plan ))))
    
    //Check if groups are valid and sum of ETCS equals 180
    isValidCourseGroup bnsGroup courseBase
    && isValidCourseGroup tcGroup courseBase
    && isValidCourseGroup ppsGroup courseBase
    && Set.forall ep rest && sumECTS plan courseBase = 180


// Tests, all should be true
let result1 = isValidCourseDesc ("02157 Funktionsprogrammering",5) = true
let result2 = isValidCourseDesc ("01005 Matematik 1",17) = false

let result3 = isValidCourseBase (Map.ofList [(02157,("Funktionsprogrammering",5)); (01005,("Matematik 1", 17))]) <> true
let result4 = isValidCourseBase (Map.ofList [(02157,("Funktionsprogrammering",5)); (01005,("Matematik 1 og ekstra", 20))])

let result5 = disjoint (set [6; 4; 3]) (set [1; 2; 7])
let result6 = disjoint (set [6; 2; 3]) (set [1; 2; 7]) <> true

let result7 = sumECTS  (set [02157; 01005]) (Map.ofList [(02157,("Funktionsprogrammering",5)); (01005,("Matematik 1", 17))]) = 22
let result8 = sumECTS  (set [02157; 01005]) (Map.ofList [(06413,("Avanceret objektorientered C#",5));(02157,("Funktionsprogrammering",5)); (01005,("Matematik 1", 17))]) = 22
let result9 = sumECTS  (set [02157; 01005; 62413]) (Map.ofList [(62413,("Avanceret objektorientered C#",5));(02157,("Funktionsprogrammering",5)); (01005,("Matematik 1", 17))]) = 27

let testCourseBase = (Map.ofList [(62413,("Avanceret objektorientered C#",5));(02157,("Funktionsprogrammering",5)); (01005,("Matematik 1", 17))])

let result10 = isValidCourseGroup ((set [02157]),(set [01005])) testCourseBase <> true

