﻿//Frederik Skovbæk Adamsen s123157
//Mathias Petersen s144874
//Nicolai Hansen s133974
//This script was developed using triple programming (as opposed to pair programming), and as such every member roughly contributed equally

// Part 1 - types
type name = string
// no = phonenumber
type no = string
// yb = year of birth
type yb = int
// ths = interests
type ths = string list
// Data for the employee containing phoneNumber, year and interest.
type description = no * yb * ths
// Employee consisting of a name and data.
type employee = name * description
// Register is a list of eomployees.
type register = employee list
// Predicate takes a employee and return true if criteria is met.
type predicate = employee -> bool

//Part 2 - declarations
// Register with 3 Employees.
let reg : register = 
    [("Frederik Adamsen",("50917285",1993,["soccer";"jazz"]));
     ("John Doe",("12345678",1995,["jazz"]));
     ("Mathias Pedersen",("12310823717",1995,["jazz"]));
     ("Mathias Pedersen d. 2",("12310823717",1994,["jazz";"pølser"]));
     ("Jacob Jepsen",("12310823717",1900,["soccer"]));
     ("Jon Tvermose",("12310823717",1980,["soccer";"jazz"]));
     ("Jane Doe",("98765432",1996,["soccer"]))]

// function to find intersection between 2 ths, returns ths.
let intersectThs(ths1: ths) (ths2: ths) :ths = 
    let set1 = Set.ofList ths1
    let set2 = Set.ofList ths2
    Set.intersect set1 set2 
    |> Set.toList

// function to check if the size of an intersect is greater than 0 (used to check if _ANY_ interest intersect with the predicates interests)
let itfOr (intersection: ths) size = 
    intersection.Length > 0

// function to check if the size of an intersect is equal to a given size (used to check if _ALL_ ths' from an employee are present in a predicate)
let itfAnd (intersection: ths) size =
    intersection.Length = size


// Helper function for parametize the predicates.
let stdP yeah (interest:ths) itf (m: employee) =
    match m with
    | (name, (no, yb, ths)) -> yb >= yeah && itf (intersectThs ths interest) interest.Length

    
// predicates for arrangements
let p1 = stdP 1982 ["soccer";"jazz"] itfAnd // Both soccer and jazz
let p2 = stdP 1982 ["soccer";"jazz"] itfOr // either

// answer is the name and phonenumber of an employee
type answer = name * no
// function that takes an employee and returns an answer
let filterNameNumber (r: employee) = 
    match r with
    | (name, (no, yb, ths)) ->
        ((name,no):answer)

// employeeToAnswer maps an employee to an answer
type employeeToAnswer = employee -> answer
// function for extracting from register with the mapping function as parameter.
let extractRegister (f:employeeToAnswer) (p:predicate) (r: register) =
    r
    |> List.filter p
    |> List.map f


//extract interested
//type: &
let extractInterested = extractRegister filterNameNumber

//Part 4 
//Tests, should all return true
let result1 = extractInterested p2 reg = [("Frederik Adamsen", "50917285"); ("John Doe", "12345678"); ("Mathias Pedersen", "12310823717"); ("Mathias Pedersen d. 2", "12310823717"); ("Jane Doe", "98765432")];;
let result2 = extractInterested p1 reg = [("Frederik Adamsen", "50917285")];;

let result3 = intersectThs ["soccer";"jazz"] ["jazz"] = ["jazz"]
let result4 = intersectThs ["soccer";"jazz";"pølser"] ["jazz";"soccer"] = ["jazz";"soccer"]
let result5 = intersectThs ["soccer";"jazz";"biler"] ["jazz";"soccer";"biler";"pølser"] = ["biler";"jazz";"soccer"]

let result6 = itfOr ["soccer";"jazz"] 2 = true
let result7 = itfAnd ["soccer";"jazz"] 3 <> true

let result8 = stdP 1982 ["soccer";"jazz"] itfOr ("Frederik Adamsen",("50917285",1993,["soccer";"jazz"])) = true
let result9 = stdP 1982 ["soccer";"jazz"] itfOr ("Frederik Adamsen",("50917285",1980,["soccer";"jazz"])) = false

let result10 = filterNameNumber ("Frederik Adamsen",("50917285",1993,["soccer";"jazz"])) = ("Frederik Adamsen","50917285")
let result11 = filterNameNumber ("Frederik Adamsen",("123123",1993,["soccer";"jazz"])) <> ("Frederik Adamsen","50917285")

let result12 = extractRegister filterNameNumber (stdP 1982 ["soccer"] itfAnd) reg = [("Frederik Adamsen", "50917285"); ("Jane Doe", "98765432")]
let result13 = extractRegister filterNameNumber (stdP 1980 ["soccer"] itfOr) reg =   [("Frederik Adamsen", "50917285"); ("Jon Tvermose", "12310823717");
   ("Jane Doe", "98765432")]

