﻿//Eksamenssæt 2017 22. dec
type Term = | V of string | C of int | F of string * Term list

let rec isGround term =
    match term with
    | V _ -> true
    | C _ -> true
    | F (_,y) -> if List.isEmpty y then true else false

//if List.isEmpty y then false else isGround Term

let term1 = V "x"
let term2 = C 3
let term3 = F("f0", [])
let term4 = F("f1", [C 3; F("f0", [])])
let term5 = F("max",[V "x";C 3])

let res1 = isGround term1
let res2 = isGround term2
let res3 = isGround term3
let res4 = isGround term4
let res5 = isGround term5

//Tagged values
type Shape = | Circle of float | Square of float | Triangle of float*float*float

let area = function
    | Circle r -> System.Math.PI * r * r
    | Square a -> a * a
    | Triangle (a,b,c) ->
        let s = (a + b + c) / 2.0
        sqrt(s*(s-a)*(s-b)*(s-c))

let resx1 = area (Circle 1.2)
let resx2 = area (Triangle(3.0,4.0,5.0))

let isShape = function
    | Circle r -> r > 0.0
    | Square a -> a > 0.0
    | Triangle (a,b,c) ->
        a > 0.0 && b > 0.0 && c > 0.0
        && a < b + c && b < c + a && c < a + b

let resx3 = isShape (Circle -1.0)
let resx4 = isShape (Square -2.0)
let resx5 = isShape (Triangle (3.0, 4.0, 7.5))


//Problem 1
//1.
let rec f1 a b = if a > b then f1 (a-b) b else a

let res11 = f1 23 3

let rec f2 a b = if a > b then 1 + f2 (a-b) b else 0

let res12 = f2 23 3

//Problem 2
//1.
type Species = string
type Location = string
type Time = int
type Observation = Species * Location * Time

let os = [("Owl","L1",3); ("Sparrow", "L2", 4); ("Eagle","L3",5); ("Falcon", "L2", 7); ("Sparrow","L1",9); ("Eagle","L1",14)]


let locationsOf (s:Species) (os:Observation list) =
    let matchingSpecies (n:Observation) =
        let (x,_,_) = n
        if x.Equals s then true else false

    let extractLocation (o:Observation) =
        let (_,x,_) = o
        x

    List.filter matchingSpecies os |> List.map extractLocation

let res21 = locationsOf "Sparrow" os
let res22 = locationsOf "Eagle" os

//2.
type Count<'a when 'a:equality> = ('a * int) list

